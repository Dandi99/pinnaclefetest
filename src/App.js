import React from 'react';
import Routes from 'components/navigator/routes';

const App = () => {
  return (
    <>
      <Routes />
    </>
  );
};

export default App;
