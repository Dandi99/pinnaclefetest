import React, { Component } from 'react';
import CountriesAPI from 'services/apis/countries';
import BooksApi from 'services/apis/books';

class BookForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      author: '',
      isbn: '',
      publishedOn: '',
      numberOfPages: '',
      country: '',
      countries: [],
      errorTitle: '',
      errorAuthor: '',
      errorIsbn: '',
      errorPublished: '',
      errorPages: '',
      isError: false,
      isErrorTitle: false,
      isErrorAuthor: false,
      isErrorIsbn: false,
      isErrorPublished: false,
      isErrorPages: false,
      isFullfilled: false,
      isButtonDisabled: false
    };
  }
  
  openForm = () => {
    document.getElementById("bookform").className="form-activated";
  }

  closeForm = () => {
    document.getElementById("bookform").className="form-deactivated";
    this.setState({
      title: '',
      author: '',
      isbn: '',
      publishedOn: '',
      numberOfPages: '',
      country: '',
      errorTitle: '',
      errorAuthor: '',
      errorIsbn: '',
      errorPublished: '',
      errorPages: '',
      isError: false,
      isErrorTitle: false,
      isErrorAuthor: false,
      isErrorIsbn: false,
      isErrorPublished: false,
      isErrorPages: false,
      isFullfilled: false,
      isButtonDisabled: false
    });
  }

  toogleDisabled = () => {
    const { isFullfilled } = this.state;

    if(isFullfilled){
      this.setState(
        {
          isButtonDisabled: true
        }
      );
    } else {
      this.setState(
        {
          isButtonDisabled: false
        }
      );
    }
  };

  componentDidMount = () => {
    CountriesAPI.getAllCountries(countries => {
      this.setState({
        countries
      });
    });
  };

  inputHandlerCountry = event => {
    const { name, value } = event.target;
    this.setState(
      {
        [name]: value,
        isError: false
      },
      this.isFormFilled()
    );
  };

  inputHandlerTitle = event => {
    const { name, value } = event.target;

    var letters = /^[a-zA-Z0-9 ]*$/;
    if(value.match(letters)){
      this.setState(
        {
          [name]: value,
          isError: false,
          isErrorTitle: false
        },
        this.isFormFilled()
      );
    } else {
      this.setState({
        isError: true,
        isErrorTitle: true,
        errorTitle: "*Only allowing letters, numbers, and spaces"
      })
    }
  };

  inputHandlerAuthor = event => {
    const { name, value } = event.target;

    var letters = /^[a-zA-Z ]*$/;
    if(value.match(letters)){
      this.setState(
        {
          [name]: value,
          isError: false,
          isErrorAuthor: false
        },
        this.isFormFilled()
      );
    } else {
      this.setState({
        isError: true,
        errorAuthor: "**Only allowing letters and spaces",
        isErrorAuthor: true
      })
    }
  };

  inputHandlerIsbn = event => {
    const { name, value } = event.target;

    var letters = /^[a-zA-Z0-9-]*$/;
    if(value.match(letters)){
      this.setState(
        {
          [name]: value,
          isError: false,
          isErrorIsbn: false
        },
        this.isFormFilled()
      );
    } else {
      this.setState({
        isError: true,
        errorIsbn: "***Only allowing letters, numbers, and dashes",
        isErrorIsbn: true
      })
    }
  };

  inputHandlerPublished = event => {
    const { name, value } = event.target;

    var letters = /(\d{4})[-](\d{1,2})[-](\d{1,2})\s*/;
    if(value.match(letters)){
      this.setState(
        {
          [name]: value,
          isError: false,
          isErrorPublished: false
        },
        this.isFormFilled()
      );
    } else {
      this.setState({
        isError: true,
        errorPublished: "****Only allowing date formats (YYYY-MM-DD)",
        isErrorPublished: true
      })
    }
  };

  inputHandlerPage = event => {
    const { name, value } = event.target;

    var letters = /^[0-9]*$/;
    if(value.match(letters)){
      this.setState(
        {
          [name]: value,
          isError: false,
          isErrorPages: false
        },
        this.isFormFilled()
      );
    } else {
      this.setState({
        isError: true,
        errorPages: "*****Only allowing numbers",
        isErrorPages: true
      })
    }
  };

  isFormFilled = () => {
    const { title, author, isbn, publishedOn } = this.state;
    console.log(this.state);
    if (title && author && isbn && publishedOn ) {
      this.setState({
        isFullfilled: true
      });
    } else {
      this.setState({
        isFullfilled: false
      });
    }
  };

  mapToDropdownOptions = items => {
    return items.map(item => {
      return (
        <option value={`${item.name}`} key={item.id}>
        </option>
      );
    });
  };

  submitForm = () => {
    const {
      title,
      author,
      isbn,
      publishedOn,
      numberOfPages,
      country,     
      isFullfilled, 
      isError
    } = this.state;
    if(isFullfilled && !isError){
      this.toogleDisabled();
      BooksApi.submitForm(
        {
          title,
          author,
          isbn,
          publishedOn,
          numberOfPages,
          country,
        }
      );
    } else {
      console.log("failed")
    }
  }

  spanError = (item) => {
    const { isErrorTitle, isErrorAuthor, isErrorIsbn, isErrorPublished, isErrorPages } = this.state;
    if(item===1 && isErrorTitle){
      return (
        <span className="span-error">*</span>
      )
    } else if(item===2 && isErrorAuthor){
      return (
        <span className="span-error">**</span>
      )
    } else if(item===3 && isErrorIsbn){
      return (
        <span className="span-error">***</span>
      )
    } else if(item===4 && isErrorPublished){
      return (
        <span className="span-error">****</span>
      )
    } else if(item===5 && isErrorPages){
      return (
        <span className="span-error">*****</span>
      )
    }
  }

  renderButton = () => {
    const { isButtonDisabled } = this.state;

    return (
      <button
        type="button"
        className="book-form-footer-submit-btn"
        block
        onClick={this.submitForm}
        disabled={isButtonDisabled}
      >
        <b>Submit</b>
      </button>
    )
  };

  renderErrorMessage = (item) => {
    const { isErrorTitle, isErrorAuthor, isErrorIsbn, isErrorPublished, isErrorPages, errorTitle, errorAuthor, errorIsbn, errorPublished, errorPages } = this.state;

    if(item===1 && isErrorTitle){
      return (
        <p className="book-form-footer-error-text">{errorTitle}</p>
      )
    } else if(item===2 && isErrorAuthor){
      return (
        <p className="book-form-footer-error-text">{errorAuthor}</p>
      )
    } else if(item===3 && isErrorIsbn){
      return (
        <p className="book-form-footer-error-text">{errorIsbn}</p>
      )
    } else if(item===4 && isErrorPublished){
      return (
        <p className="book-form-footer-error-text">{errorPublished}</p>
      )
    } else if(item===5 && isErrorPages){
      return (
        <p className="book-form-footer-error-text">{errorPages}</p>
      )
    }
  }

  renderForm = () => {
    const { countries } = this.state;

    return (
      <form className="book-form"  name="addBook">
        <div className="book-form-header">
          <h4 className="book-form-header-text">Add Book</h4>
          <button type="button" onClick={this.closeForm} className="book-form-header-btn"><i class="fas fa-times fa-2x book-form-header-btn-close"></i></button>
        </div>
        <div className="book-form-group">
          <p className="book-form-group-label">Title {this.spanError(1)}</p>
          <input type="text" name="title" placeholder="e.g Fleishman Is In Trouble: A Novel" id="title" onChange={this.inputHandlerTitle} required />

          <p className="book-form-group-label">Author {this.spanError(2)}</p>
          <input type="text" name="author" placeholder="e.g Fleishman Is In Trouble: A Novel" id="author" onChange={this.inputHandlerAuthor} required />

          <p className="book-form-group-label">ISBN {this.spanError(3)}</p>
          <input type="text" name="isbn" placeholder="e.g Fleishman Is In Trouble: A Novel" id="isbn" onChange={this.inputHandlerIsbn} required />

          <p className="book-form-group-label">Published On {this.spanError(4)}</p>
          <input type="text" name="publishedOn" placeholder="e.g Fleishman Is In Trouble: A Novel" id="published" onChange={this.inputHandlerPublished} required />

          <p className="book-form-group-label">Number of Page {this.spanError(5)}</p>
          <input type="text" name="numberOfPages" placeholder="e.g Fleishman Is In Trouble: A Novel" id="pages" onChange={this.inputHandlerPage} required />

          <p className="book-form-group-label">Country Publisher</p>
          <input list="countries" name="country" placeholder="e.g Fleishman Is In Trouble: A Novel" id="country" onChange={this.inputHandlerCountry} required></input>
          <datalist id="countries">
            {this.mapToDropdownOptions(countries)}
          </datalist>
        </div>

        <div className="book-form-footer">
          <div className="book-form-footer-error">
            {this.renderErrorMessage(1)}
            {this.renderErrorMessage(2)}
            {this.renderErrorMessage(3)}
            {this.renderErrorMessage(4)}
            {this.renderErrorMessage(5)}
          </div>
          <div className="book-form-footer-submit">
            {this.renderButton()}
          </div>
        </div>
      </form>
    );
  };

  render() {
    return (
      <div>
        <button className="book-form-button" type="button" onClick={this.openForm}><b>Add +</b></button>
        <div id="bookform" className="form-deactivated">
          {this.renderForm()}
        </div>
      </div>
    );
  }
}

export default BookForm;