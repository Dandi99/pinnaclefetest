import React from 'react';
import { shallow } from 'enzyme';
import BookForm from '..';

describe('BookForm', () => {
  it('renders correctly', () => {
    const component = shallow(<BookForm />);
    expect(component).toMatchSnapshot();
  });
});
