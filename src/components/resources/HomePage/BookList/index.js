import React, { Component } from 'react';
import BooksApi from 'services/apis/books';
import BookCard from 'components/common/BookCard';
import BookForm from '../BookForm';

class BookList extends Component {
  constructor(props){
    super(props);
    this.state = {
      books : [],
      limit : 4,
      loadMore : false
    }
  }

  componentDidMount = () => {
    BooksApi.getAllBooks(books => {
      this.setState({
        books
      });
    });
  };

  renderButton = () => {
    const { loadMore } = this.state;
    if (loadMore) {
      return (
        <button 
          id="loadmore" 
          className="book-list-footer-btn" 
          type="button" 
          onClick={this.LoadMore}
        >
          <b>Minimize</b>
        </button>
      );
    } else {
      return (
        <button 
          id="loadmore" 
          className="book-list-footer-btn" 
          type="button" 
          onClick={this.LoadMore}
        >
          <b>Load More</b>
        </button>
      );
    }
  }

  LoadMore = () => {
    const { books, loadMore } = this.state;
    if(loadMore){
      this.setState({
        limit: 4,
        loadMore: false
      })
    } else {
      this.setState({
        limit: books.length,
        loadMore: true
      })
    }
  }

  render() {
    const { books, limit } = this.state;

    return (
      <div className="book-list">
        <div className="book-list-header">
          <div className="book-list-header-left">
            <p className="book-list-header-left-text"><b>Books ({books.length})</b></p>
          </div>
          <div className="book-list-header-right">
            <BookForm />
          </div>
        </div>
        <div className="book-list-grid">
          {books.slice(0,limit).map(book => {
            return (
              <BookCard
                key={book.id}
                title={book.title}
                author={book.author}
                isbn={book.isbn}
                publishedOn={book.publishedOn}
                numberOfPages={book.numberOfPages}
                country={book.country}
                imageUrl={book.imageUrl}
                limit = {limit}
              />
            );
          })}
        </div>
        <div className="book-list-footer">
          {this.renderButton()}
        </div>
      </div>
    );
  }
}

export default BookList;