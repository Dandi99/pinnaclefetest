import React from 'react';
import { shallow } from 'enzyme';
import BookList from '..';

describe('BookList', () => {
  it('renders correctly', () => {
    const component = shallow(<BookList />);
    expect(component).toMatchSnapshot();
  });
});
