import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HomePage from 'scenes/HomePage';

const Routes = () => (
  <Router>
    <main className="app__content">
      <Route
        exact
        path="/"
        render={props => <HomePage {...props} type="" />}
      />

    </main>
  </Router>
);

export default Routes;
