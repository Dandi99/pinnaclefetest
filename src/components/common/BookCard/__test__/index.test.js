import React from 'react';
import { shallow } from 'enzyme';
import BookCard from '..';

describe('BookCard', () => {
  it('renders correctly', () => {
    const component = shallow(<BookCard />);
    expect(component).toMatchSnapshot();
  });
});
