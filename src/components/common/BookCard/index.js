import React from 'react';
import img from 'assets/img/img.svg';

const BookCard = props => {
  const { title, author, isbn, publishedOn, numberOfPages, country  } = props;

  return (
    <div className="book-card">
      <div className="book-card-text">
        <h4>{title}</h4>
        <p>Book by {author}</p>
        <div className="book-card-text-detail">
          <div className="book-card-text-detail-left">
            <p className="book-card-text-detail-head">ISBN</p>
            <p className="book-card-text-detail-content">{isbn}</p>
            <p className="book-card-text-detail-head">published on</p>
            <p className="book-card-text-detail-content">{publishedOn}</p>
          </div>
          <div className="book-card-text-detail-right">
            <p className="book-card-text-detail-head">Number of Pages</p>
            <p className="book-card-text-detail-content">{numberOfPages}</p>
            <p className="book-card-text-detail-head">Country Publisher</p>
            <p className="book-card-text-detail-content">{country}</p>
          </div>
        </div>
      </div>
      <div className="book-card-poster">
        <img src={img} className="book-card-poster-img" alt="movie" />
      </div>
    </div>
  );
};

export default BookCard;
