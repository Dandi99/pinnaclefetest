import { withRouter } from 'react-router-dom';
import Header from 'components/common/Header';
import BookList from 'components/resources/HomePage/BookList';
import React, { Component } from 'react';

class HomePage extends Component {
  render() {
    return (
      <>
        <Header />
        <BookList />
      </>
    );
  }
}

export default withRouter(HomePage);
