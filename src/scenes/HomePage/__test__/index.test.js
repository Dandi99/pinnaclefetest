import React from 'react';
import { shallow } from 'enzyme';
import HomePage from '..';

describe('HomePage', () => {
  it('renders correctly', () => {
    const component = shallow(<HomePage />);
    expect(component).toMatchSnapshot();
  });
});
