import axios from 'axios';
import BaseApi from 'services/apis/base-api';
import { API_ROOT } from 'services/apis/config';

class BooksApi extends BaseApi {
  static ROOT_URL = `${API_ROOT}/books`;

  static getAllBooks = (success, error = () => {}) => {
    const url = `${this.ROOT_URL}`;

    axios
      .get(url)
      .then(response => success(response.data))
      .catch(e => error(e));
  };

  static getBookById = (id, success, error = () => {}) => {
    const url = `${this.ROOT_URL}/${id}`;

    axios
      .get(url, {
        params: {
          id
        }
      })
      .then(response => success(response.data))
      .catch(e => error(e));
  };

  static submitForm = (form, success, error = () => {}) => {
    const url = `${this.ROOT_URL}`;

    axios
      .post(url, form)
      .then(response => success(response.data))
      .catch(e => error(e));
  };

}

export default BooksApi;
