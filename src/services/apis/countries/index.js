import axios from 'axios';
import BaseApi from 'services/apis/base-api';
import { API_ROOT } from 'services/apis/config';

class CountriesApi extends BaseApi {
  static ROOT_URL = `${API_ROOT}/countries`;

  static getAllCountries = (success, error = () => {}) => {
    const url = `${this.ROOT_URL}`;

    axios
      .get(url)
      .then(response => success(response.data))
      .catch(e => error(e));
  };

}

export default CountriesApi;
