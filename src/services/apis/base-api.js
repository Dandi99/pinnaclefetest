import axios from 'axios';

class BaseApi {
  static ROOT_URL = '';

  static success(callback, data) {
    callback(data);
  }

  static error(callback, data) {
    if (data.response) {
      callback(data.response.data);
    } else {
      callback({ errors: [data.message] });
    }
  }

  static get(params, success, error = () => {}) {
    return axios
      .get(`${this.ROOT_URL}`, {
        params
      })
      .then(response => {
        this.success(success, response.data.data);
      })
      .catch(err => {
        this.error(error, err);
      });
  }

  static post(item, success, error = () => {}) {
    return axios
      .post(this.ROOT_URL, item)
      .then(response => {
        success(response.data.data);
      })
      .catch(err => {
        this.error(error, err);
      });
  }
}

export default BaseApi;
